﻿

function ratingclick(row, col) {
            
                var strID = "row" + row + "col" + i;
                 
                $("#ratingItem" + row).attr("data-rating", col);
                
                var im1 = "/Content/images/starblank.png";
                var im2 = "/Content/images/starblue.png";

                for (var i = 1; i <= 10; i++) {
                    strID = "row" + row + "col" + i;
     
                    var myTextField = document.getElementById(strID);
                    if (i <= col) {
                        myTextField.setAttribute("src", im2);
                    }
                    else
                    {
                        myTextField.setAttribute("src", im1);
                    }
                }
}

function ApplyRatings() {
    

    var data = {};
    $(".ratingItem").each(function (index, item) {
 
        var strKey = $(this).attr("data-player");
        var strRating = $(this).attr("data-rating");
       
        data["playerRatings[" + index + "].Key"] = strKey;
        data["playerRatings[" + index + "].Value"] = strRating;
    });


    //Makes ajax call to controller
    $.ajax({
        type: "POST",
        data: data,
        url: "/Player/RateMatch",
        success: function (message) {
            window.location.href = '/player/start';
        }
    });

}
