﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using rmp.Models;
namespace rmp
{
    public static class renders
    {
        public static IHtmlString BigButton(string text,string url)
        {
            TagBuilder t = new TagBuilder("a");
            t.Attributes.Add("href", url);
            t.Attributes.Add("class", "btn_vertical"); 
            t.SetInnerText(text);

            return new MvcHtmlString(t.ToString(TagRenderMode.Normal));
        }

        public static IHtmlString rating_item(PlayerProfilModel player,int rowIndex,int value=0)
        {

            TagBuilder t = new TagBuilder("label");
            t.Attributes.Add("style", "font-size:25px");    
            t.SetInnerText(player.nick);
            
            return new MvcHtmlString(
            
                @"<tr><td class='col1' style='width:100px'><div class='ratingItem' id='ratingItem" + rowIndex + "'  data-player='" + player.id.ToString() + "' data-rating='" + value + "' >"  
                
                
                + t.ToString() + "</td><td>" +
                                        rating_star("1", rowIndex, (1 <= value) )+
                                        rating_star("2", rowIndex, (2 <= value)) +
                                        rating_star("3", rowIndex, (3 <= value)) +
                                        rating_star("4", rowIndex, (4 <= value)) +
                                        rating_star("5", rowIndex, (5 <= value)) +
                                        rating_star("6", rowIndex, (6 <= value)) +
                                        rating_star("7", rowIndex, (7 <= value)) +
                                        rating_star("8", rowIndex, (8 <= value)) +
                                        rating_star("9", rowIndex, (9 <= value)) +
                                        rating_star("10", rowIndex, (10 <= value)) +
                                        "</div></td><tr>"
                                        );

        }


        public static string rating_star(string colIndex,int rowIndex,bool isON=false )
        {
            
            TagBuilder t1 = new TagBuilder("img");
            t1.Attributes.Add("style", "margin:5px");
            if (isON) { t1.Attributes.Add("src", rmp.vars.starON); } else { t1.Attributes.Add("src", rmp.vars.starOFF); }
            t1.Attributes.Add("id", "row" + rowIndex + "col" + colIndex );
            t1.Attributes.Add("onclick", "ratingclick('" + rowIndex + "','" + colIndex +  "')");
            t1.Attributes.Add("val1", colIndex);
            //t1.SetInnerText(  rowIndex.ToString()  );
            return t1.ToString();

        }

        public static IHtmlString GetRatingTable(List <PlayerProfilModel>players,string title,string title2 )
        {
            string innerHtml = "";


            innerHtml = "<tr><th colspan='2'>" + title + "</th></tr>";
             

            int rowIndex = 0;
            foreach (PlayerProfilModel player in players)
            {
                rowIndex ++;
                innerHtml += rating_item(player , rowIndex, player.lastRating  ).ToString();
            }
            string strHtml = @"<table class='ratingitem' tyle='height:40px;' >" + innerHtml + @"</table>";
            return new MvcHtmlString(strHtml);

        }



 
    }
}
