﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using rmp.Models;


namespace rmp.Controllers
{
    [Authorize]
    public class PlayerController : Controller
    {
        //
        // GET: /Igrac/

        public ActionResult Index()
        {
            return View();
        }

       
        public ActionResult Profil()
        {
            PlayerStats stats = db.GetPlayerStats(db.currentUser.id );

            return View(stats);
        }

        public ActionResult Start()
        {
            return View( );
        }

        [AllowAnonymous ]
        public ActionResult logIn()
        {
            return View();
        }

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult logIn(rmp.Models.LogInModel model)
        {
            db.currentUser = db.GetUser(model.username, model.password);

            if (ModelState.IsValid)
            {
                if (db.currentUser !=null)
                {
                   FormsAuthentication.SetAuthCookie(model.username, false);
                   return RedirectToAction("Start");

                }
                else
                {
                    ModelState.AddModelError("", "Invalid username or password");
                }
            }

            return View();
        }

        [AllowAnonymous]
        public ActionResult RegisterPlayer()
        {
            return View();
        }

        
        [HttpPost]
        [AllowAnonymous]
        public ActionResult RegisterPlayer(rmp.Models.RegisterModel model)
        {
            db.currentUser = db.EditCreatePlayer(model.UserName, model.Email, model.Mobile, model.Password, model.Nick);
             
                if (ModelState.IsValid)
                {
                    if (db.currentUser != null)
                    {
                        FormsAuthentication.SetAuthCookie(model.UserName, false);
                        return RedirectToAction("Start");

                    }
                    else
                    {
                        ModelState.AddModelError("", "Invalid username or password");
                    }
                }

                return View();

            }


            public ActionResult JoinTeam()
            {
                return View();
            }

            [HttpPost]
            public ActionResult JoinTeam(TeamPlayerRegister p)
            {
                
                if(!db.TeamPasswordIsValid(p.teamID ,p.password )){
                    ModelState.AddModelError("", "Error! ");
                    return View();
                }

                if (db.JoinTeam(db.currentUser.id ,p.teamID  ))
                {
                   return RedirectToAction("Start", "Player");
                }
                else
                {
                    ModelState.AddModelError("", "Error! ");
                    return View();
                }


            }

            public ActionResult pickSquad()
            {
                rmp.Models.PickSquadModel model = new PickSquadModel(db.currentUser.id);
                model.rute = "picksquad";
                return View(model);
            }

            

            public ActionResult PickMatch(string teamID="",bool WantToJoin=false )
            {
                if (teamID == "") teamID = rmp.db.currentSquad.Id.ToString();

                db.currentSquad = db.GetTeam(Convert.ToInt32(teamID));
                PickMatchModel pm = new PickMatchModel();
                if(WantToJoin ){pm.nextAction="JoinMatch";}
                    else {pm.nextAction = "RateMatch";}
                pm.squadName = db.currentSquad.Name;
                pm.matches  = db.GetMatches(db.currentSquad.Id);

                return View(pm );

            }
  

            public ActionResult RateMatch(string matchID="")
            {
                db.currentMatch = db.GetMatch(Convert.ToInt32(matchID));
                rmp.Models.RateMatchModel rm = new Models.RateMatchModel();
                rm.players = db.GetPlayersFromMatch(db.currentMatch.match_id);
                rmp.db.LoadPlayerRatings(ref rm.players, db.currentUser.id, Convert.ToInt32(matchID));
                rm.destinationLink = @"ApplyRatings()";
                rm.matchName = db.currentMatch.name;
                return View(rm);
            }


            [HttpPost]
            public ActionResult RateMatch(Dictionary<string, string> playerRatings)
            {
                foreach ( KeyValuePair<string, string> player in playerRatings )
                {
                    int intRating = Convert.ToInt32(player.Value.ToString());
                    int intPlayerID = Convert.ToInt32(player.Key.ToString());
                    db.RatePlayer(db.currentMatch.match_id, intPlayerID, db.currentUser.id, intRating);

                }

                return RedirectToAction("PickMatch");
            }


           
            public ActionResult JoinMatch(string matchID="0")
            {
                db.JoinMatch(db.currentUser.id, Convert.ToInt32(matchID));
                return RedirectToAction("Start", "Team", new { teamID = db.currentSquad.Id });

            }
        
        
        }

    


    }

