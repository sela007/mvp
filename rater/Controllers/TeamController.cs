﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using rmp.Models;
namespace rmp.Views.Team
{
    [Authorize ]
    public class TeamController : Controller
    {
        //
        // GET: /Team/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Profile(string teamID="")
        {

            rmp.Models.TeamModel tim = db.GetTeam( Convert.ToInt32(teamID ));
            db.LoadPlayersAndMatches(ref tim);
            
            return View(tim);


        }

        public ActionResult Start(int teamID)
        {
            db.currentSquad = db.GetTeam(teamID);
            return View(db.currentSquad );
        }

    

        public ActionResult Ratings()
        {
            rmp.Models.TeamRatingsModel model = new Models.TeamRatingsModel();
            model.squad = db.currentSquad;
            model.matches = db.GetMatches(db.currentSquad.Id );
            foreach (rmp.Models.MatchModel match in model.matches)
            {
                match.players = db.GetPlayersAndStats(match.match_id);
            }
            return View(model);
        }



        public ActionResult NewMatch()
        {
            NewMatchModel model2 = new NewMatchModel();
            model2.squadName = db.currentSquad.Name;
            model2.players = new List<AddPlayersModel>();
            foreach (PlayerProfilModel p in db.GetPlayers(db.currentSquad.Id))
            {
                model2.players.Add(new AddPlayersModel { name = p.nick, id = p.id, selected = false });
            }
              

            return View(model2);
        }

        [HttpPost ]
        public ActionResult NewMatch(NewMatchModel model)
        {
            int matchID = db.CreateMatch(model.matchName, db.currentSquad.Id, DateTime.Now );
            foreach (AddPlayersModel player in model.players)
            {
                if (player.selected) db.JoinMatch(player.id, matchID);
            }


            return RedirectToAction("Start", "Team", new { teamID = db.currentSquad.Id });
        }


        public ActionResult NewTeam()
        {
            
            return View();
        }

        [HttpPost]
        public ActionResult NewTeam(NewTeamModel model)
        {

            if (db.CreateTeam(model.TeamName, model.Password, db.currentUser.id)>-1)
            {
                return RedirectToAction("PickSquad", "Player");

            }
            else
            {
                ModelState.AddModelError("", "Error while updating database");
                return View(model);
            }

            
        }

    }
}
