﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using rmp.Models;

namespace rmp.Models
{   
    
    public class MatchModel
    {
        public int match_id;
        public string name;
        public DateTime date;
        public int team_id;
        public int rating;
        public List<PlayerProfilModel> players;


    }

    public class PickMatchModel
    {
        public string squadName { get; set; }
        public List<MatchModel> matches { get; set; }
        public string nextController { get; set; }
        public string nextAction { get; set; }
    }

   

}
