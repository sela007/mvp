﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
namespace rmp.Models
{
    public class TeamModel
    {
        
        public int Id { get; set; }
        public string Name { get; set; }     
        public int AdminId { get; set; }
        public List<PlayerProfilModel> Players { get; set; }
        public List<PlayerProfilModel> PlayersLastMatch { get; set; }
        public List<MatchModel> Matches { get; set; }

    }

    public class TeamPlayerRegister
    {
      

        [Required]
        public int teamID { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string password { get; set; }

    }

    public class NewMatchModel
    {

        [Required]
        [Display(Name = "Name")]
        public string matchName { get; set; }

        public string squadName{ get; set; }
       
     
        public DateTime date { get; set; }

        public List<AddPlayersModel> players { get; set; }
        
    }

    public class AddPlayersModel
    {
        public string name { get; set; }
        public int id { get; set; }
        public bool selected { get; set; }
    }


    public class RateMatchModel
    {
        public string matchName;
        public List<PlayerProfilModel> players;
        public string destinationLink;

    }

    public class PickSquadModel
    {
        public PickSquadModel(int playerID)
        {
            player = db.GetPlayer(playerID);
            squads = db.GetTeams(playerID);
        }
        public List<TeamModel> squads;
        public PlayerProfilModel player;
        public string rute;

         
    }

    public class TeamRatingsModel
    {
        public TeamModel squad;
        public List<MatchModel> matches;

    }

    public class NewTeamModel
    {
       
            [Required]
            [Display(Name = "Squad name")]
            public string TeamName { get; set; }


            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [System.Web.Mvc.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }
 

    }

    public class JoinMatchModel
    {
        [Required]
        [Display(Name = "Match ID")]
        public string UserName { get; set; }


        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

    }

}
