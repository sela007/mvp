﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace rmp.Models
{
    public class PlayerProfilModel
    {
            public int id { get; set; }
            public string username { get; set; } 

            public string nick { get; set; }
            
            public string mobile { get; set; }
            public string mail { get; set; }
             
            public string password { get; set; }



            public int lastRating;
            public int averageRating;


        
    }

    public class PlayerStats
    {
        public PlayerStats()
        {
            Matches = new List<PlayerMatchRatings>();
            
        }
        public decimal AvgRating { get; set; }
        public List<PlayerMatchRatings > Matches { get; set; }

    }
    public class PlayerMatchRatings
    {
        public PlayerMatchRatings()
        {
            GivenRatings = new Dictionary<int, int>();
        }
        public string MatchName { get; set; }
        public int MatchID { get; set; }
        public Dictionary<int,int> GivenRatings { get; set; }

    }

}
