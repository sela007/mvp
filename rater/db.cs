﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using rmp.Models;
using System.Drawing;
namespace rmp
{
    public static class db
    {
        public static string connstring = @"workstation id=raterdb.mssql.somee.com;packet size=4096;user id=sela007_SQLLogin_1;pwd=emylolr5rx;data source=raterdb.mssql.somee.com;persist security info=False;initial catalog=raterdb";

        //public static string connstring = @"Data Source=LAPTOP\sqlexpress;Initial Catalog=rmp;Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
        public static SqlConnection conn;
        public static rmp.Models.PlayerProfilModel currentUser { get; set; }
        public static rmp.Models.MatchModel currentMatch { get; set; }
        public static rmp.Models.TeamModel currentSquad { get; set; }

        public static PlayerProfilModel GetUser(string username,string password)
        {
            string strSql = "SELECT player_id,username,mail,mobile,nick FROM [players] WHERE username='" + username + "' AND password='" + password + "'";
            conn = new SqlConnection(rmp.db.connstring);
            SqlCommand command = new SqlCommand(strSql, conn);
            conn.Open();
            SqlDataReader reader = command.ExecuteReader();
            PlayerProfilModel i = new PlayerProfilModel();

            if (reader.HasRows)
            {
                reader.Read();
                i.id = reader.GetInt32(0);
                i.username = reader.GetString(1);
                i.mail = reader.GetString(2);
                i.mobile = reader.GetString(3);
                i.nick = reader.GetString(4);
                conn.Close();
                return i;
            }
            else
            {
                conn.Close();
                return null;
            }
        }

        public static List<TeamModel> GetTeams(int playerID   )
        {
            List<TeamModel> teams=new List<TeamModel>();
            string strSql = @"SELECT teams.team_id
                            FROM teams
                            INNER JOIN player_team
                            ON teams.team_id=player_team.team_id
                            WHERE player_team.player_id = " + playerID;
                
            conn = new SqlConnection(rmp.db.connstring);
            SqlCommand command = new SqlCommand(strSql, conn);
            conn.Open();
            SqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read() == true)
                {
                    TeamModel t = new TeamModel();
                    t = GetTeam(reader.GetInt32(0));
                    teams.Add(t);
                }

                
            }

           conn.Close();
           return teams;

        }


        public static TeamModel GetTeam(int teamID)
        {
            TeamModel t = new TeamModel();
            string strSql = "SELECT team_id, name, master_admin_id FROM teams WHERE team_id='" +teamID.ToString() + "'";
            conn = new SqlConnection(rmp.db.connstring);
            SqlCommand command = new SqlCommand(strSql, conn);
            conn.Open();
            SqlDataReader reader = command.ExecuteReader();
         
            if (reader.HasRows)
            {
                reader.Read();
                t.Id = reader.GetInt32(0);
                t.Name = reader.GetString(1);
                t.AdminId  = reader.GetInt32(2);
                conn.Close();
                return t;
            }
            else
            {
                conn.Close();
                return null;
            }
        }


        public static PlayerProfilModel  GetPlayer(int playerID)
        {
            PlayerProfilModel  p = new PlayerProfilModel ();
            string strSql = "SELECT player_id, username, mail,mobile, nick FROM players WHERE player_id='" + playerID.ToString() + "'";
            conn = new SqlConnection(rmp.db.connstring);
            SqlCommand command = new SqlCommand(strSql, conn);
            conn.Open();
            SqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                reader.Read();
                p.id = reader.GetInt32(0);
                p.username = reader.GetString(1);
                p.mail = reader.GetString(2);
                p.mobile = reader.GetString(3);
                p.nick = reader.GetString(4);
                conn.Close();
                return p;
            }
            else
            {
                conn.Close();
                return null;
            }
        }
        public static List< PlayerProfilModel>  GetPlayers(int teamID)
        {
            
            string strSql = @"SELECT players.player_id, username, mail, mobile, nick
                            FROM players
                            INNER JOIN player_team
                            ON players.player_id=player_team.player_id
                            WHERE player_team.team_id = " + teamID.ToString();

            return GetPlayersFromDB(strSql);

        }

        public static List<PlayerProfilModel> GetPlayersFromMatch(int matchID)
        {
            List<PlayerProfilModel> players = new List<PlayerProfilModel>();

            string strSql = @"SELECT players.player_id, players.username, players.mail, players.mobile, players.nick
                               FROM players INNER JOIN player_match 
                               ON player_match.player_id = players.player_id 
                               WHERE (player_match.match_id = '" + matchID.ToString () + "')";

            players= GetPlayersFromDB(strSql);

            
            return players;
        }


        public static List<PlayerProfilModel> GetPlayersAndStats(int matchID)
        {
            List<PlayerProfilModel> players = new List<PlayerProfilModel>();

            string strSql = @"select 
            players.player_id , 
            players.username,
            players.nick,
            players.mail,
            players.mobile,
            AVG(ratings.rating) AS avgRating 
            from ratings 
            RIGHT JOIN players on players.player_id = ratings.player_id 
            where ratings.match_id=" + matchID.ToString () + " group by players.player_id,players.username,players.nick,players.mail,players.mobile order by avgRating DESC";


             conn = new SqlConnection(rmp.db.connstring);
            SqlCommand command = new SqlCommand(strSql, conn);
            conn.Open();
            SqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read() == true)
                {
                    PlayerProfilModel p = new PlayerProfilModel();
                    p.id = reader.GetInt32(0);
                    p.username = reader.GetString(1);
                    p.nick = reader.GetString(2);
                    p.mail = reader.GetString(3);
                    p.mobile = reader.GetString(4);
                    p.lastRating = reader.GetInt32(5);
                    players.Add(p);
                }

            }
            conn.Close();
            return players;

        }


        public static MatchModel GetMatch(int matchID,bool loadPlayers=false )
        {
            MatchModel m = new MatchModel();
            string strSql = @"SELECT match_id, name, date FROM matches WHERE match_id='" + matchID.ToString() + "'";
            conn = new SqlConnection(rmp.db.connstring);
            SqlCommand command = new SqlCommand(strSql, conn);
            conn.Open();
            SqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                reader.Read();
                m.match_id = reader.GetInt32(0);
                m.name  = reader.GetString(1);
                m.date   = reader.GetDateTime (2);
                
                conn.Close();
                if(loadPlayers ) m.players = GetPlayersFromMatch(matchID);
                return m;
            }
            else
            {
                conn.Close();
                return null;
            }

            

        }

        public static List<MatchModel > GetMatches(int teamIndex)
        {
            List<MatchModel > matches = new List<MatchModel >();
            string strSql = @"SELECT matches.match_id, matches.name , matches.date  
                            FROM matches INNER JOIN team_match 
                            ON matches.match_id=team_match.match_id
                            WHERE team_match.team_id = " + teamIndex.ToString() +  " ORDER BY matches.date DESC";

            conn = new SqlConnection(rmp.db.connstring);
            SqlCommand command = new SqlCommand(strSql, conn);
            conn.Open();
            SqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read() == true)
                {
                    MatchModel m = new MatchModel();
                    m.match_id = reader.GetInt32(0);
                    m.name = reader.GetString(1);
                    m.date = reader.GetDateTime(2);
                    m.team_id = teamIndex ;
                   
                    matches.Add(m);
                }

                conn.Close();
                return matches;
            }

            return matches;

        }
        public static void LoadPlayersAndMatches(ref TeamModel t)
        {
            t.Matches = GetMatches(t.Id );
            t.Players = GetPlayers(t.Id);

            if (t.Matches.Count > 0)
            {
                t.PlayersLastMatch = GetPlayersFromMatch(t.Matches[0].match_id);
            }
            else { t.PlayersLastMatch = new List<PlayerProfilModel>(); }


        }

        public static void LoadPlayerAvgRatings(ref List<PlayerProfilModel> players, int intMatchID)
        {
            foreach (PlayerProfilModel player in players)
            {
                player.lastRating = GetAvgRating(player.id, intMatchID);
            }
        }

        public static void LoadPlayerRatings(ref List<PlayerProfilModel> players,  int raterID,int intMatchID)
        {
            foreach (PlayerProfilModel player in players)
            {
                player.lastRating = GetRating(player.id,raterID, intMatchID);
            }
        }

        public static int GetAvgRating(int playerID, int intMatchID)
        {
            int intRating = 0;
            string strSql = "";
            strSql = @"SELECT AVG(rating) AS AverageRating
                            FROM ratings WHERE match_id=" + intMatchID.ToString() + " AND player_id=" + playerID.ToString();
           
            conn = new SqlConnection(rmp.db.connstring);
            SqlCommand command = new SqlCommand(strSql, conn);
            conn.Open();
            SqlDataReader reader = command.ExecuteReader();

            MatchModel m = new MatchModel();
            if (reader.HasRows)
            {
                reader.Read();
                try
                {
                    intRating = reader.GetInt32(0);
                }
                catch { }
            }
            conn.Close();
            return intRating;
        }

        public static int GetRating(int playerID, int raterID, int intMatchID)
        {
            int intRating = 0;
            string strSql = "";
            strSql = @"SELECT rating FROM ratings WHERE match_id=" + intMatchID.ToString() + " AND player_id=" + playerID.ToString() + " AND rater_id=" + raterID.ToString();

            conn = new SqlConnection(rmp.db.connstring);
            SqlCommand command = new SqlCommand(strSql, conn);
            conn.Open();
            SqlDataReader reader = command.ExecuteReader();

            MatchModel m = new MatchModel();
            if (reader.HasRows)
            {
                reader.Read();
                try
                {
                    intRating = reader.GetInt32(0);
                }
                catch { }
            }
            conn.Close();
            return intRating;
        }


        public static MatchModel GetLastMatch(int teamID)
        {
            string strSql = @"SELECT TOP 1 matches.match_id, matches.name , matches.date  
                            FROM matches INNER JOIN team_match 
                            ON matches.match_id=team_match.match_id
                            WHERE team_match.team_id = " + teamID.ToString() + " ORDER BY matches.date DESC";
            
            conn = new SqlConnection(rmp.db.connstring);
            SqlCommand command = new SqlCommand(strSql, conn);
            conn.Open();
            SqlDataReader reader = command.ExecuteReader();

            MatchModel m = new MatchModel();
            if (reader.HasRows)
            {  
                    m.match_id = reader.GetInt32(0);
                    m.name = reader.GetString(1);
                    m.date = reader.GetDateTime(2);
                    m.team_id = teamID;    
                
            }
            conn.Close();
            return m;
        }

        public static int GetLastMatchID(int teamID)
        {
            int matchID = -1;
            string strSql = @"SELECT TOP 1 matches.match_id
                            FROM matches INNER JOIN team_match 
                            ON matches.match_id=team_match.match_id
                            WHERE team_match.team_id = " + teamID.ToString() + " ORDER BY matches.date DESC";

            conn = new SqlConnection(rmp.db.connstring);
            SqlCommand command = new SqlCommand(strSql, conn);
            conn.Open();
            SqlDataReader reader = command.ExecuteReader();

            MatchModel m = new MatchModel();
            if (reader.HasRows)
            {
                m.match_id = reader.GetInt32(0);
                m.name = reader.GetString(1);
                m.date = reader.GetDateTime(2);
                m.team_id = teamID;    
            }
            conn.Close();
            return matchID;
        }

       
     

        private static List<PlayerProfilModel> GetPlayersFromDB(string strSql)
        {
            List<PlayerProfilModel> players = new List<PlayerProfilModel>();
            conn = new SqlConnection(rmp.db.connstring);
            SqlCommand command = new SqlCommand(strSql, conn);
            conn.Open();
            SqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read() == true)
                {
                    PlayerProfilModel p = new PlayerProfilModel();
                    p.id = reader.GetInt32(0);
                    p.username = reader.GetString(1);
                    p.mail = reader.GetString(2);
                    p.mobile = reader.GetString(3);
                    p.nick = reader.GetString(4);

                    players.Add(p);
                }

            }
            conn.Close();
            return players;
        }

        public static PlayerProfilModel  EditCreatePlayer(string username, string newMail, string newMobile, string newPassword, string newNick)
        {
            PlayerProfilModel player = new PlayerProfilModel();
            player.username = username;
            player.mail = newMail;
            player.mobile = newMobile;
            player.nick = newNick;
            player.password = newPassword;
            return EditCreatePlayer(player);
        }

        public static PlayerProfilModel EditCreatePlayer(PlayerProfilModel player)
        {
            if (PlayerExist(player.username))
            {
                return null;
            }
            else
            {
                try
                {
                    string strSql = @"INSERT INTO players (username, mail, mobile, password, nick)
                                VALUES (@username,@mail,@mobile,@password,@nick)";
                    conn = new SqlConnection(rmp.db.connstring);

                    SqlCommand command = new SqlCommand(strSql, conn);
                    command.Parameters.AddWithValue("username", player.username);
                    command.Parameters.AddWithValue("mail", player.mail);
                    command.Parameters.AddWithValue("mobile", player.mobile);
                    command.Parameters.AddWithValue("password", player.password);
                    command.Parameters.AddWithValue("nick", player.nick);

                    conn.Open();
                    command.ExecuteNonQuery();
                    conn.Close();
                    return player;
                }
                catch
                {
                    conn.Close();
                    return null;
                }
            }

        }

        public static bool PlayerExist(string username)
        {
            string strSql = "SELECT username FROM players WHERE username='" + username + "'";
            conn = new SqlConnection(rmp.db.connstring);
            SqlCommand command = new SqlCommand(strSql, conn);
            conn.Open();
            if (command.ExecuteScalar() == null) { conn.Close(); return false; } else { conn.Close(); return true; }

        }

        public static bool TeamPasswordIsValid(int teamID,string password)
        {
            string strSql = "SELECT password FROM teams WHERE team_id = " + teamID.ToString()  ;
            conn = new SqlConnection(rmp.db.connstring);
            SqlCommand command = new SqlCommand(strSql, conn);
            conn.Open();
            try
            {
                if (command.ExecuteScalar().ToString() == password) { conn.Close(); return true; } else { conn.Close(); return false; }
            }
            catch { conn.Close(); return false; }
        }

        public static bool JoinTeam(int player_id,int team_id)
        {
            try
            {
                string strSql = @"INSERT INTO player_team (player_id, team_id)
                                VALUES (@playerid,@teamid)";
                conn = new SqlConnection(rmp.db.connstring);

                SqlCommand command = new SqlCommand(strSql, conn);
                command.Parameters.AddWithValue("playerid", player_id );
                command.Parameters.AddWithValue("teamid", team_id );

                conn.Open();
                command.ExecuteNonQuery();
                conn.Close();
                return true;
            }
            catch
            {

                return false;
            }

        }
        public static bool RecordExist(string strSql)
        {     
            conn = new SqlConnection(rmp.db.connstring);
            SqlCommand command = new SqlCommand(strSql, conn);
            conn.Open();
            if (command.ExecuteScalar() == null) { conn.Close(); return false; } else { conn.Close(); return true; }
        }
        public static bool Execute(string strSql)
        {
            try{
            conn = new SqlConnection(rmp.db.connstring);
            SqlCommand command = new SqlCommand(strSql, conn);
            conn.Open();
            command.ExecuteNonQuery();
            conn.Close();
                return true ;
            }catch(Exception e){
                string sex = e.Message;
                return false;
            }
        }
     
        public static void RatePlayer(int match_id, int player_id, int rater_id, int rating,string comment="")  
        {
          

                string strDelete = @"DELETE FROM ratings WHERE 
                                rater_id=" + rater_id.ToString() +
                                 "AND player_id=" + player_id.ToString() +
                                 "AND match_id=" + match_id;
                 //ako već postoji rating onda briši
                Execute(strDelete);

                DateTime date = DateTime.Now;

                try
                {
                    string strSql = @"INSERT INTO ratings (match_id, player_id, rater_id, rating,comment, date)
                                VALUES (@match_id,@player_id,@rater_id,@rating,@comment,@date)";

                    conn = new SqlConnection(rmp.db.connstring);

                    SqlCommand command = new SqlCommand(strSql, conn);
                    command.Parameters.AddWithValue("match_id", match_id);
                    command.Parameters.AddWithValue("player_id", player_id);
                    command.Parameters.AddWithValue("rater_id", rater_id);
                    command.Parameters.AddWithValue("rating", rating);
                    command.Parameters.AddWithValue("comment", comment);
                    command.Parameters.AddWithValue("date", date);

                    conn.Open();
                    command.ExecuteNonQuery();  
                }
                catch{}     

                conn.Close();
        }

        public static int CreateTeam(string teamName,string password, int adminID)
        {
            try
            {
                string strSql = @"INSERT INTO teams (name,password,master_admin_id)
                                OUTPUT Inserted.team_id
                                VALUES (@name,@password,@admin)";

                conn = new SqlConnection(rmp.db.connstring);

                SqlCommand command = new SqlCommand(strSql, conn);
                command.Parameters.AddWithValue("name", teamName);
                command.Parameters.AddWithValue("password", password);
                command.Parameters.AddWithValue("admin", adminID);

                conn.Open();
                int i = Convert.ToInt32(command.ExecuteScalar());
                conn.Close();

                JoinTeam(adminID, i);
                return i;
            }
            catch { conn.Close(); return -1; }
        }

        public static int CreateMatch(string matchName, int teamID, DateTime date)
        {

            int matchID = NewMatch(matchName, date);
            if (matchID > 0)
            {

                string strSql = @"INSERT INTO team_match (team_id,match_id)
                                VALUES (@teamID,@matchID)";

                conn = new SqlConnection(rmp.db.connstring);

                SqlCommand command = new SqlCommand(strSql, conn);
                command.Parameters.AddWithValue("teamID", teamID );
                command.Parameters.AddWithValue("matchID", matchID );
                 
                conn.Open();
                command.ExecuteNonQuery();
                conn.Close();
            }
            return matchID;

        }

        private static int NewMatch(string matchName, DateTime date)
        {
            string strSql = @"INSERT INTO matches (name,[date])
                            OUTPUT Inserted.match_id
                            VALUES(@name,@date)";
            conn = new SqlConnection(rmp.db.connstring);
            SqlCommand command = new SqlCommand(strSql, conn);
            command.Parameters.AddWithValue("name", matchName);
            command.Parameters.AddWithValue("date", date);
         
            conn.Open();
            int i = Convert.ToInt32(command.ExecuteScalar());
            conn.Close();
            return i;


        }


        public static void JoinMatch(int playerID,int matchID)
        {

                string strSql = @"INSERT INTO player_match (player_id,match_id)
                                VALUES (@playerID,@matchID)";

                conn = new SqlConnection(rmp.db.connstring);
                conn.Open();

                SqlCommand command = new SqlCommand(strSql, conn);

                command.Parameters.AddWithValue("playerID", playerID );
                command.Parameters.AddWithValue("matchID", matchID);
                try
                {
                    command.ExecuteNonQuery();
                

                strSql = @"INSERT INTO ratings (player_id,rater_id,match_id,rating,[date])
                                VALUES (@playerID,@raterID,@matchID,@rating,@date)";


                command = new SqlCommand(strSql, conn);
                command.Parameters.AddWithValue("playerID", playerID);
                command.Parameters.AddWithValue("raterID", db.currentUser.id );
                command.Parameters.AddWithValue("matchID", matchID);
                command.Parameters.AddWithValue("rating", 0);
                command.Parameters.AddWithValue("date", DateTime.Now );
                
                    command.ExecuteNonQuery();
                }
                catch { }

                conn.Close();
           
        }



        public static  PlayerStats GetPlayerStats(int playerID)
        {
            PlayerStats stats = new PlayerStats();

            string strSql = @"select  rating, ratings.match_id ,mtc.name, COUNT(*) as RatingCount,mtc.date 
                            from ratings   
                            inner join matches as mtc on ratings.match_id = mtc.match_id
                            where player_id=@player_id
                            group by ratings.match_id,rating,mtc.name,mtc.date order by mtc.date desc";

            conn = new SqlConnection(rmp.db.connstring);
            SqlCommand command = new SqlCommand(strSql, conn);
            command.Parameters.AddWithValue("player_id", playerID);
            conn.Open();
            SqlDataAdapter adp = new SqlDataAdapter(command);

            DataTable tbl = new System.Data.DataTable();
            adp.Fill(tbl);

        

            Dictionary<int, int> added=new Dictionary<int,int> ();

            int pos = -1;
            foreach (DataRow dr in tbl.Rows)
            {
                int id = Convert.ToInt32(dr[1]);
                if (!added.ContainsKey(id))
                {
                    pos += 1;
                    added.Add(id, pos);
                    PlayerMatchRatings m = new PlayerMatchRatings();
                    m.MatchID = Convert.ToInt32(dr[1]);
                    m.MatchName = dr[2].ToString();
                    stats.Matches.Add(m);
                }
            }

            foreach (DataRow dr in tbl.Rows)
            {
                int matchID = Convert.ToInt32(dr[1]);

                int rating = Convert.ToInt32(dr[0]);
                int ratingCount = Convert.ToInt32(dr[3]);
                stats.Matches[added[matchID]].GivenRatings.Add(rating, ratingCount);
 
            }

            tbl.Dispose();
            PlayerMatchRatings match = new PlayerMatchRatings();
            conn.Close();
            return stats;
        }

        public static Bitmap GetStats(int playerID)
        {
            Bitmap bmp = new Bitmap(200,100);


            return bmp;

        }
        

    }
}
